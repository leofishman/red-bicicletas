const express = require('express');
const router = express.Router();
const bicicletaControllerApi = require('../../controllers/api/bicicletaControllerApi')

router.get('/', bicicletaControllerApi.bicicleta_list)
router.post('/create', bicicletaControllerApi.bicicleta_create)
router.put('/:code/update',bicicletaControllerApi.bicicleta_update_post)
router.delete('/:code/delete', bicicletaControllerApi.bicicleta_delete)

module.exports = router